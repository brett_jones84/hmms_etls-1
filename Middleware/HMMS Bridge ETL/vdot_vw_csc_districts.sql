USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_csc_districts') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_csc_districts]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vdot_vw_csc_districts] AS
SELECT distinct
      [districtID]
      ,[districtName]
      ,[districtPhone]
  FROM [$(MAIN_DB)].[dbo].[vdot_vw_csc_residencies]

GO
