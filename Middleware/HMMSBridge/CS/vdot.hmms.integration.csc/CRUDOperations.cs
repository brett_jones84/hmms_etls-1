﻿using System;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Linq;
using System.Configuration;
using System.Data.Services;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using VDOT.CSC.Data.XrmComponents;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;


using System.Collections.Generic;
using System.Net;
using Microsoft.Xrm.Client.Configuration;

namespace Microsoft.Crm.Sdk.Samples
{
    /// <summary>
    /// entity operations like add VueWorks ID to Service Request, Update Service Request Status, and Add Comment to Service Request,
    /// </summary>
    /// <remarks>
    /// You can run this from the console for local testing or from the web service project for full testing..</remarks>
    public class CRUDOperations
    {
        private Guid _vdot_servicerequestid;
        private OrganizationService _serviceProxy;
        private ServerConnection.Configuration _config;
        private Microsoft.Xrm.Client.CrmConnection _connection;
        private string connectionName = "MyOnPremiseConnection";

        //when set to true, config files must have proper credentials.
        //when set to false, application must be running under an account that has proper credentials.
        private bool isLocal = true;
        public CRUDOperations_Result initializationResult = new CRUDOperations_Result();
        
        public CRUDOperations_Result update_VDOT_Status(string vdot_SRID, string stringStatusCode)
        {

            stringStatusCode = stringStatusCode.Replace(" ", "");
            stringStatusCode = stringStatusCode.ToLower();
            int statusCode;
            bool intParseResult = Int32.TryParse(stringStatusCode, out statusCode);

            try
            {
                //use this line for connection string 
                //using (var _serviceProxy = new OrganizationService(this._connection))
                
                //use this line to use the credentials of the service account running the code.
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                
                    {
                        int statusCodeFromEnum = -99;
                        //if the statuscode that was passed in was text then use it as the key of the enum.
                        if (!intParseResult)
                        {
                            statusCodeFromEnum = (int) Enum.Parse(typeof(servicerequest_statuscode), stringStatusCode);
                        }
                        else
                        {
                            
                            //check to see if the int supplied is actually in the enum
                            if (Enum.IsDefined(typeof(servicerequest_statuscode), statusCode))
                            {
                                statusCodeFromEnum = statusCode;
                            }
                            else
                            {
                                throw new System.ArgumentException("Parameter must be a valid value", stringStatusCode);
                            }
                                
                        }

                        ColumnSet cols = new ColumnSet(true);
                        Guid vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);
                        vdot_servicerequest retrievedServicerequest = (vdot_servicerequest)_serviceProxy.Retrieve("vdot_servicerequest", vdot_servicerequstid, cols);
                    
                        if (statusCodeFromEnum != retrievedServicerequest.statuscode.Value)
                        {

                            retrievedServicerequest.statuscode = new OptionSetValue(statusCodeFromEnum);
                            _serviceProxy.Update(retrievedServicerequest);

                            return new CRUDOperations_Result(System.Net.HttpStatusCode.OK,
                                string.Format("Sucess {0} was successfully assigned a new status of {1}({2})",
                                    vdot_SRID,
                                    stringStatusCode, statusCodeFromEnum));
                        }
                        else
                        {


                            //return new CRUDOperations_Result(System.Net.HttpStatusCode.BadRequest,
                            //    string.Format(
                            //        "Failure {0} was Not assigned a new status of {1} ({2}). The service Request already has a status of {1}({2})",
                            //        vdot_SRID,
                            //        stringStatusCode, statusCodeFromEnum));
                            throw new System.ArgumentException(string.Format("Failure {0} was Not assigned a new status of {1} ({2}). The service Request already has a status of {1}({2})",vdot_SRID,stringStatusCode, statusCodeFromEnum));
                        }

                    }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                // You can handle an exception here or pass it back to the calling method.
                return this.HandleExceptions(ex, string.Format("Failure {0} was NOT assigned a new status of {1}", vdot_SRID, stringStatusCode));
                //throw;
            }
        }

        public CRUDOperations_Result update_VDOT_AMSID(string vdot_SRID, string vdot_AMSID)
        {
            try
            {
                //use this line for connection string 
                //using (var _serviceProxy = new OrganizationService(this._connection))
                //use this line to use the credentials of the service account running the code.
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {
                    ColumnSet cols = new ColumnSet(true);
                    Guid vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);
                    vdot_servicerequest retrievedServicerequest =
                        (vdot_servicerequest) _serviceProxy.Retrieve("vdot_servicerequest", vdot_servicerequstid, cols);
                    retrievedServicerequest.vdot_AMSID = vdot_AMSID;
                    _serviceProxy.Update(retrievedServicerequest);
                    return new CRUDOperations_Result(System.Net.HttpStatusCode.OK,
                        string.Format("Sucess {0} was successfully assigned a VueWorks ID of {1}", vdot_SRID, vdot_AMSID));
                }

            }
                // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                // You can handle an exception here or pass it back to the calling method.
                return this.HandleExceptions(ex,
                    string.Format("Failure {0} was NOT assigned a VueWorks ID of {1}", vdot_SRID, vdot_AMSID));
                //throw;
            }
        }

        public EntityCollection get_Comments(string vdot_SRID)
        {
            try
            {
                //using (var _serviceProxy = new OrganizationService(this._connection))
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {
                    ColumnSet cols = new ColumnSet(true);
                    QueryByAttribute querybyattribute = new QueryByAttribute("annotation");
                    querybyattribute.ColumnSet = cols;
                    querybyattribute.Attributes.AddRange("objectid");
                    Guid vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);
                    querybyattribute.Values.AddRange(vdot_servicerequstid);
                    EntityCollection retrieved = _serviceProxy.RetrieveMultiple(querybyattribute);
                    return retrieved;
                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                //throw the error back up to the calling function
                throw;
            }

        }
        
        public CRUDOperations_Result add_Comment(string vdot_SRID, string commentText,Nullable<DateTime> timeStamp)
        {
            try
            {
                EntityCollection commentsForThisSR = this.get_Comments(vdot_SRID);
                foreach (Annotation entity in commentsForThisSR.Entities)
                {
                    DateTime newDatetime = (DateTime) entity.CreatedOn;
                    System.Diagnostics.Debug.WriteLine(newDatetime.Ticks);
                    if (timeStamp.HasValue && (timeStamp.Value.Ticks == newDatetime.Ticks))
                    {
                        //return new CRUDOperations_Result(System.Net.HttpStatusCode.Conflict, string.Format("Failure. A comment already exists for SR {0} with time stamp: {1}", vdot_SRID, timeStamp.Value.ToString()));
                        throw new System.ArgumentException(string.Format("Failure. A comment already exists for SR {0} with time stamp: {1}", vdot_SRID, timeStamp.Value.ToString()));

                    }
                }
                //using (var _serviceProxy = new OrganizationService(this._connection))
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {
                    Guid vdot_servicerequstid = getGuidByvdot_SRID(vdot_SRID);
                    ColumnSet cols = new ColumnSet(true);
                    Entity annotation = new Entity("annotation");
                    annotation["objectid"] = new EntityReference("vdot_servicerequest", vdot_servicerequstid);
                    annotation["subject"] = "Lead Note";
                    annotation["notetext"] = commentText;
                    Guid annotationId = _serviceProxy.Create(annotation);
                    return new CRUDOperations_Result(System.Net.HttpStatusCode.OK, string.Format("Sucess A comment was added to SR {0}. Comment text :{1}", vdot_SRID, commentText));
                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                return this.HandleExceptions(ex, string.Format("Failure a new comment for SR {0} was not created. Comment text : {1}", vdot_SRID, commentText));
            }


        }

        public Guid getGuidByvdot_SRID(string vdot_SRID)
        {
            
            if (this._vdot_servicerequestid != Guid.Empty)
            {
                return this._vdot_servicerequestid;
            }
            else
            {
                
                try
                {
                    //use this line for connection string 
                    //using (var _serviceProxy = new OrganizationService(this._connection))
                    using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                    {
                        ColumnSet cols = new ColumnSet("vdot_servicerequestid");
                        QueryByAttribute querybyattribute = new QueryByAttribute("vdot_servicerequest");
                        querybyattribute.ColumnSet = cols;
                        querybyattribute.Attributes.AddRange("vdot_srid");
                        querybyattribute.Values.AddRange(vdot_SRID);
                        EntityCollection retrieved = _serviceProxy.RetrieveMultiple(querybyattribute);
                        this._vdot_servicerequestid = retrieved[0].GetAttributeValue<Guid>("vdot_servicerequestid");
                        return this._vdot_servicerequestid;

                    }

                }
                    // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
                catch (Exception ex){
                    // pass exception back to the calling method.
                    throw;
                }
            }
        }

        public void displayEntityAttributes(string entityName= "annotation")
        {
            ServerConnection.Configuration serverConfig = this._config;

            try
            {
                //using (var _serviceProxy = new OrganizationService(this._connection))
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {

                    Microsoft.Xrm.Sdk.Messages.RetrieveEntityRequest req = new Microsoft.Xrm.Sdk.Messages.RetrieveEntityRequest
                    {
                        EntityFilters = Microsoft.Xrm.Sdk.Metadata.EntityFilters.All,
                        LogicalName = entityName // like "account"
                    };
                    Microsoft.Xrm.Sdk.Messages.RetrieveEntityResponse res = (Microsoft.Xrm.Sdk.Messages.RetrieveEntityResponse)this._serviceProxy.Execute(req
                    );
                    Microsoft.Xrm.Sdk.Metadata.EntityMetadata currentEntity = res.EntityMetadata;
                    foreach (Microsoft.Xrm.Sdk.Metadata.AttributeMetadata attribute in currentEntity.Attributes)
                    {
                        var attributeName = attribute.LogicalName;
                        System.Console.WriteLine(attributeName);
                    }
                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                //pass the exception back to the calling function
                throw;
            }

        }

        public void GetOptionSet(string entityName= "vdot_servicerequest", string fieldName= "statuscode")
        {

            try
            {
                //use this line for connection string 
                //using (var _serviceProxy = new OrganizationService(this._connection)) 
                using (var _serviceProxy = CrmConfigurationManager.CreateContext(this.connectionName, true) as CrmOrganizationServiceContext)
                {

                    var attReq = new RetrieveAttributeRequest();
                    attReq.EntityLogicalName = entityName;
                    attReq.LogicalName = fieldName;
                    attReq.RetrieveAsIfPublished = true;
                    var attResponse = (RetrieveAttributeResponse)_serviceProxy.Execute(attReq);
                    var attMetadata = (EnumAttributeMetadata)attResponse.AttributeMetadata;
                    var optionList = (from o in attMetadata.OptionSet.Options
                                      select new { Value = o.Value, Text = o.Label.UserLocalizedLabel.Label }).ToList();
                    string optionListString = optionList.ToString();

                }

            }
            // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
            catch (Exception ex)
            {
                // Pass the exception back to the calling method.
                throw;
            }


        }
        /// <summary>
        /// This gets executed for console testing
        /// </summary>
        /// <param name="vdot_SRID"></param>
        /// <param name="vdot_AMSID"></param>
        public void Run(string vdot_SRID, string vdot_AMSID)
        {
            try
            {
                this.update_VDOT_AMSID(vdot_SRID, vdot_AMSID);
            }

            catch (Exception ex)
            {
                //pass the excpetion off to the handler.
                this.HandleExceptions(ex,"Error at Run");
            }
            // Additional exceptions to catch: SecurityTokenValidationException, ExpiredSecurityTokenException,
            // SecurityAccessDeniedException, MessageSecurityException, and SecurityNegotiationException.
            finally
            {
                Console.WriteLine("Press <Enter> to exit.");
                Console.ReadLine();
            }


        }
        /// <summary>
        /// initialize the object.
        /// </summary>
        public CRUDOperations()
        {
            try
            {
                //if running this application locally, you need to use a connection string and not rely on the service account for credentials.
                if (this.isLocal)
                {
                    String connectionString = GetServiceConfiguration();
                    this._connection = CrmConnection.Parse(connectionString);
                }
            }
            catch (Exception ex)
            {
             this.initializationResult = this.HandleExceptions((ex), "Error at object construction.");
            }
        }

        /// <summary>
        /// Gets web service connection information from the app.config file.
        /// If there is more than one available, only the first one is used.
        /// </summary>
        /// <returns>A string containing web service connection configuration information.</returns>
        private static String GetServiceConfiguration()
        {
            // Get available connection strings from app.config.
            int count = ConfigurationManager.ConnectionStrings.Count;

            // Create a filter list of connection strings so that we have a list of valid
            // connection strings for Microsoft Dynamics CRM only.
            List<KeyValuePair<String, String>> filteredConnectionStrings =
                new List<KeyValuePair<String, String>>();

            for (int a = 0; a < count; a++)
            {
                if (isValidConnectionString(ConfigurationManager.ConnectionStrings[a].ConnectionString))
                {
                    filteredConnectionStrings.Add
                        (new KeyValuePair<string, string>
                            (ConfigurationManager.ConnectionStrings[a].Name,
                                ConfigurationManager.ConnectionStrings[a].ConnectionString));
                }
                //else
                //{
                //    throw new ArgumentException("The connection String is invalid");
                //}
            }

            // We are only going to use this code for one specific case so just use the first entry, which must be a string connection.
            return filteredConnectionStrings[0].Value;

        }

        /// <summary>
        /// Verifies if a connection string is valid for Microsoft Dynamics CRM.
        /// </summary>
        /// <returns>True for a valid string, otherwise False.</returns>
        private static Boolean isValidConnectionString(String connectionString)
        {
            // At a minimum, a connection string must contain one of these arguments.
            if (connectionString.Contains("Url=") ||
                connectionString.Contains("Server=") ||
                connectionString.Contains("ServiceUri="))
                return true;

            return false;
        }

        private CRUDOperations_Result HandleExceptions(Exception ex0, string additionalInfo="")
        {
            CRUDOperations_Result result = new CRUDOperations_Result();
            result.text = "The application terminated with an error. " + additionalInfo;
            result.FullMessage = ex0.ToString();

            //catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
            if (ex0 is FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>)
             {
                FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex = (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>)ex0;
                result.status = HttpStatusCode.NotFound;
                result.Timestamp = ex.Detail.Timestamp;
                result.ErrorCode = ex.Detail.ErrorCode;
                result.Message = ex.Detail.Message;
                result.InnerFault = null == ex.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault";
             }

            //catch (System.TimeoutException ex)
            if(ex0 is System.TimeoutException)
            {
                result.status = HttpStatusCode.RequestTimeout;
                System.TimeoutException ex = (System.TimeoutException) ex0;
                result.Message=ex.Message;
                result.StackTrace=ex.StackTrace;
                result.InnerFault = null == ex.InnerException.Message ? "No Inner Fault" : ex.InnerException.Message;
            }


            //if the connection string was not valid throw error.
            if (ex0 is ArgumentException)
            {
                ArgumentException ex = (ArgumentException)ex0;
                result.status = HttpStatusCode.PreconditionFailed;
                result.Message = ex.Message;
                
            }


            //catch all other errors
            if (ex0 is System.Exception)
            {
                System.Exception ex = ex0;

                result.status = HttpStatusCode.InternalServerError;
                result.Message = ex.Message;
                // Display the details of the inner exception.
                if (ex.InnerException != null)
                {
                    result.InnerFault = ex.InnerException.Message;
                    FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> fe = ex.InnerException
                        as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
                    if (fe != null)
                    {
                        result.Timestamp = fe.Detail.Timestamp;
                        result.ErrorCode = fe.Detail.ErrorCode;
                        result.Message = fe.Detail.Message;
                        result.TraceText = fe.Detail.TraceText;
                        result.InnerFault = null == fe.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault";
                    }
                }
            }
            
            // Additional exceptions to catch: SecurityTokenValidationException, ExpiredSecurityTokenException,
            // SecurityAccessDeniedException, MessageSecurityException, and SecurityNegotiationException.

            return result;
        }

        #region Main method
        /// <summary>
        /// Standard Main() method used by most SDK samples.
        /// </summary>
        /// <param name="args"></param>
        static public void Main(string[] args)
        {
            CRUDOperations app = new CRUDOperations();
            app.Run("115710", "19876458");
        }

        #endregion Main method
    }
    /// <summary>
    /// Class for return types. Meant to be consumed by WEB API.
    /// </summary>
    public class CRUDOperations_Result
    {
        public System.Net.HttpStatusCode status { get; set; }
        public string text { get; set; }
        public DateTime Timestamp  { get; set; }
        public int ErrorCode  { get; set; }
        public string Message  { get; set; }
        public string TraceText  { get; set; }
        public string InnerFault  { get; set; }
        public string StackTrace  { get; set; }
        public string FullMessage { get; set; }


        public CRUDOperations_Result(System.Net.HttpStatusCode status = HttpStatusCode.BadRequest, string text="", DateTime Timestamp=default(DateTime), int ErrorCode=0, string Message="", string TraceText="", string InnerFault="", string StackTrace="",string FullMessage = "")
        {
            this.status = status;
            this.text = text;
            this.Timestamp = Timestamp;
            this.ErrorCode = ErrorCode;
            this.Message = Message;
            this.TraceText = TraceText;
            this.InnerFault = InnerFault;
            this.StackTrace = StackTrace;
            this.FullMessage = FullMessage;
        }
    }

    /// <summary>
    /// Enumeration of values for the Service Request status code
    /// </summary>
    public enum servicerequest_statuscode
    {

        [System.Runtime.Serialization.EnumMemberAttribute()]
        notapplicable = 866190010,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        needsreview = 866190006,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        assigned = 1,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        inprocess = 866190009,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        cancelrequested = 866190008,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        cancelled = 866190003,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        pending = 866190004,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        rejected = 866190007,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        closed = 866190005,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        inactive = 2,
    }
}


