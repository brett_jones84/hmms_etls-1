The following steps will create the necessary objects for the WebIMS import and run the load.  The entire job should run in less than 5 seconds based on WSQ01438 runs.  Note that the job is a full wipe/reload currently and can be run multiple times.

Step 1: Ensure person executing scripts has the following permissions in the wsq01438 databases...

	a) create a table in the HMMS_STAGING database
	b) edit table data in HMMS and HMMS_STAGING
	c) create procedure in HMMS
	d) create and assign owner_login_name for job ( https://msdn.microsoft.com/en-us/library/ms187901.aspx ).  The job must also be configured to run as a login that has permissions to execute SQL Agent jobs.
	e) read data from CSC_MSCRM database (for views)

Step 2: Ensure that all files are in the same directory:

Create_and_Populate_StockGroups_Table.sql
Create_IMSDataLoad_Proc.sql
Create_vdot_vw_csc_districts.View.sql
Create_vdot_vw_csc_residencies.View.sql
Create_vdot_vw_csc_workAreas.View.sql
Create_vdot_vw_stock_locations.View.sql
Create_WebIMSDataLoad_Job.sql
main.bat
main.sql

Step 3: Run main.bat (this executes main.sql which calls most of the necessasary steps).  the output from this is appended to results.log

Step 4: In SQL Mgmt Studio, Run Create_WebIMSDataLoad_Job.sql

Step 5: In SQL Mgmt Studio, Execute "WebIMSDataLoad" Job