USE $(MAIN_DB)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

PRINT 'Updating Equipment Permission Groups...'
go

UPDATE [dbo].[tbl_ResMgr_Permission_Groups]
SET ForEquipment = 1
WHERE Name = 'Bristol' 
	or Name = 'Culpeper' 
	or Name = 'Central Office'
	or Name = 'Hampton Roads'
	or Name = 'Fredericksburg'
	or Name = 'Richmond'
	or Name = 'Staunton'
	or Name = 'Northern Virginia'
	or Name = 'Lynchburg'
	or Name = 'Salem'
	
PRINT '...done'
go
