USE $(MAIN_DB)
GO

-- PRINT '$(MAIN_DB)'
-- PRINT '$(STAGING_DB)'
-- PRINT '$(MSCRM)'

/****** 
--		Object:  	StoredProcedure [dbo].[EquipmentLoad]    
--		Author: 	Brett Jones (WorldView Solutions)
--		History: 	Version 1.0,	10/4/2017, 	Brett Jones
--					Initial creation of ETL process for pulling M5 Equipment data from the HMMS_STAGING database.
******/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('EquipmentDataLoad') IS NOT NULL
	BEGIN
		PRINT N'Dropping existing EquipmentDataLoad Procedure...';  
		DROP PROCEDURE  [dbo].[EquipmentDataLoad];
	END
GO

--PRINT N'Creating LaborDataLoad Procedure...';  
CREATE procedure [dbo].[EquipmentDataLoad] 
	@cleanLoad INT = 0 --if 1, wipe any existing data 
as
begin

PRINT N'EquipmentDataLoad Procedure Beginning...';  
BEGIN TRANSACTION [Tran1]

BEGIN TRY

	IF @cleanLoad = 1
		BEGIN

			PRINT N'cleanLoad param is set.  Purging existing data...';
			
			PRINT N'purging tbl_ResMgr_Equipment_Categories...';  
			DELETE
			FROM [dbo].[tbl_ResMgr_Equipment_Categories];
			
			PRINT N'purging tbl_ResMgr_Equipment...';  
			DELETE
			FROM [dbo].[tbl_ResMgr_Equipment];

			PRINT N'purging tbl_ResMgr_Equipment_ExData...';  
			DELETE
			FROM [dbo].[tbl_ResMgr_Equipment_ExData]
		END

	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Equipment_Categories  -----------------
	-----------------------------------------------------------------
	PRINT N'Processing tbl_ResMgr_Equipment_Categories...'; 

	set identity_insert dbo.tbl_ResMgr_Equipment_Categories ON;
	
	insert into tbl_ResMgr_Equipment_Categories (Name, Description)
	select distinct CATEGORY
	from [$(STAGING_DB)].[dbo].[M5_HMMS_EQUIPMENTS];

	set identity_insert dbo.tbl_ResMgr_Equipment_Categories OFF;
		
	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Equipment  ------------------------
	-----------------------------------------------------------------
	PRINT N'Processing tbl_ResMgr_Equipment...';  

	set identity_insert dbo.tbl_ResMgr_Equipment ON;

	merge tbl_ResMgr_Equipment as Target
	using (

	SELECT	s.UNIT_NO, s.CATEGORY_DESC, s.CATEGORY, s.CARDINAL_DEPT_ID
					, c.Category_ID, c.Name
					, d.DEPARTMENTID, d.DESCRIPTION, d.DISTRICTNAME
					, pg.Perm_ID, pg.Name
			FROM [$(STAGING_DB)].[dbo].[M5_HMMS_EQUIPMENTS] s
			LEFT JOIN tbl_ResMgr_Equipment_Categories c
				on c.Name as CName = s.CATEGORY
			LEFT JOIN DEB_CARDINAL_DEPARTMENTTREE d
				on d.DEPARTMENTID = s.CARDINAL_DEPT_ID
			LEFT JOIN tbl_ResMgr_Permission_Groups pg
				on pg.name as DName = d.DISTRICTNAME
				
			)

	on (target.Equipment_ID = source.UNIT_NO)

	insert (Name, Equipment_ID, External_ID, department_id, Type_ID, Available, newrecord, perm_ID, updatedDate, Category_ID)
	values (source.CATEGORY_DESC, source.UNIT_NO, source.UNIT_NO, -1, 1, 1, 0, source.perm_ID, CURRENT_TIMESTAMP, source.CName)
	;

	set identity_insert dbo.tbl_ResMgr_Equipment OFF;


	-----------------------------------------------------------------
	-----------------  tbl_ResMgr_Equipment_ExData  -----------------
	-----------------------------------------------------------------
	PRINT N'Processing tbl_ResMgr_Equipment_ExData...';  

	merge tbl_ResMgr_Equipment_ExData as Target
	using (
			SELECT UNIT_NO, CATEGORY_DESC, CATEGORY, M5_DEPT_NO, M5_DEPT_DESCRIPTION, CARDINAL_DEPT_ID, LEASE_RATE, LEASE_TERM
			FROM $(STAGING_DB).DBO.M5_HMMS_EQUIPMENTS
		) as Source
	on (target.Equipment_ID = source.UNIT_NO)

	insert (Equipment_ID, Text1, Text2, Text3, Text5)
	values (source.UNIT_NO, source.M5_DEPT_NO, source.CARDINAL_DEPT_ID, source.M5_DEPT_DESCRIPTION, source.CATEGORY)
	;
 
	PRINT N'Committing transaction...'; 
	COMMIT TRANSACTION [Tran1];

END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION [Tran1];

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

	--TODO: email?

	-- return error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
END CATCH  

PRINT N'...EquipmentDataLoad Complete'; 

END
 
GO


