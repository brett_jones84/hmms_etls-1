USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_A21_Labor') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_A21_Labor]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO












CREATE VIEW [dbo].[vdot_vw_A21_Labor] AS
SELECT wo.ID, wo.WorkOrderID AS Work_Order_Id
			--, wodf.Disaster AS Is_Disaster
			,case
				when wodf.Disaster = 1 then 'YES'
				when wodf.Disaster = 0 then 'NO'
				else 'NO'
			end AS Is_Disaster
			, CASE WHEN wo.GroupID = 147 THEN 'Projects' ELSE 'CSC' END AS Speedtype
			, CAST(wo.BeginDate AS Date) AS Date_Of_Work
			, SUBSTRING(wodf.[Department ID], 1, 5) AS Department_ID
			, wodf.[Department ID] AS Dept_Description
			,CASE WHEN wo.GroupID = 147 THEN '0' ELSE CONCAT(SUBSTRING(wo.DepartmentName, 1, 3), SUBSTRING(wodf.[System], 1, 1), SUBSTRING(wo.GroupName, 1, 4)) 
			END AS Cost_Center_Code
			, CASE WHEN wo.GroupID = 147 THEN wodf.[UPC Number] ELSE '' END AS Project
			, CASE WHEN wo.GroupID = 147 THEN '-1' ELSE SUBSTRING(wo.ActivityDescription, 1, 5) END AS Task_Code
			, CASE WHEN wo.GroupID = 147 THEN '' ELSE wo.ActivityDescription END AS Task_Description
			, CASE WHEN wo.GroupID = 147 THEN SUBSTRING(wo.ActivityDescription, 1, 3) ELSE '' END AS Activity_Code			
			, CASE WHEN wo.GroupID = 147 THEN wo.ActivityDescription ELSE '' END AS Activity_Description
			, wodf.[County Code (FIPS Code)] AS FIPS 
			, wodf.[AU1 (If applicable)] AS Agency1__AU
			, wodf.[AU2 (Route)] AS Agency2__AU2
			--, wodf.Asset as Asset
			, wodf.[State Structure No.] as Asset
			--the rest of the view changes based on what type of resource you are looking at.
			, 'LABOR' AS Resource_Type
			, ru.EmployeeName AS Resource
			, ru.RateName AS Time_Recording_Charge --this needs to be fixed...
			, ru.EmployeeID as Employee_ID
			, ru.Hours AS Resource_Quantity
			, 'hours' AS Resource_UOM
			, '' AS Stock_Location
			, '' AS NIGP_CODE
			, '' as Condition
			

FROM 
	reports.WorkOrders AS wo 	
	LEFT JOIN reports.WorkOrderForm_MaintenanceWorkOrderForm AS wodf ON wo.ID = wodf.ID
	LEFT JOIN reports.ResourceUsage as ru ON wo.ID = ru.Record_ID

where wodf.[Department ID] is not null and ru.EmployeeID != -1










GO
