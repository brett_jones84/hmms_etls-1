USE [$(MAIN_DB)]
GO

/****** 
--		Object:  	StoredProcedure [dbo].[COALoad]    
--		Author: 	Michael Kolonay (WorldView Solutions)
--		History: 	Version 1.0,	9/18/2017, 	Michael Kolonay
--					Initial creation of ETL process for pulling Chart of Accounts data from the HMMS_STAGING database.
******/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('COALoad') IS NOT NULL
	BEGIN
		PRINT N'Dropping existing COALoad Procedure...';  
		DROP PROCEDURE  [dbo].[COALoad];
	END
GO

--PRINT N'Creating LaborDataLoad Procedure...';  
CREATE procedure [dbo].[COALoad] 
	@cleanLoad INT = 0 --if 1, wipe any existing data 
as
begin

PRINT N'COALoad Procedure Beginning...';  
BEGIN TRANSACTION [Tran1]

BEGIN TRY

	IF @cleanLoad = 1
		BEGIN

			PRINT N'cleanLoad param is set.  Purging existing data...';
			
			PRINT N'purging AU1 from tbl_Maintenance_Activity_UserListItems AU Items...';  
---------------------------------------------------------
			--Fiscal - AU1
			--ID 61
			DELETE
			FROM dbo.tbl_Maintenance_Activity_UserListItems where list_id = 61;


		PRINT N'purging Departments from tbl_Maintenance_Activity_UserListItems Department Items...';  
---------------------------------------------------------
			--Fiscal - Departments
			--ID 50
			DELETE
			FROM dbo.tbl_Maintenance_Activity_UserListItems where list_id = 50;

		END


	-----------------------------------------------------------------
	-----------------  tbl_Maintenance_Activity_UserListItems  -----------------
	-----------------------------------------------------------------
	---------------------------------------------------------
	--Fiscal - AU1
	--ID 61
	--select [TMOCMART.STRUCTURE].[FUNCTIONCD] from $(STAGING_DB).dbo.[TMOCMART.FUNCTION];
	PRINT N'Processing tbl_Maintenance_Activity_UserListItems AU Items...'; 
	---------------------------------------------------------
	MERGE dbo.tbl_Maintenance_Activity_UserListItems AS TARGET 
	using (SELECT '#'+FUNCTIONCD as item
	FROM [$(STAGING_DB)].[dbo].[DEB_CARDINAL_FUNCTION] where [FUNCTIONCD] is not null) AS Source 
	ON ( target.item = source.item and target.list_id=61) 
	WHEN matched AND target.item <> source.item THEN 
	UPDATE SET Target.item = source.item 
	WHEN NOT matched BY target THEN 
	INSERT (list_id, item) 
	VALUES ( 61,
		Source.item); 
	select * from dbo.tbl_Maintenance_Activity_UserListItems where list_id = 61;

	---------------------------------------------------------
	-----------------------------------------------------------------
	-----------------  tbl_Maintenance_Activity_UserListItems  -----------------
	-----------------------------------------------------------------
	---------------------------------------------------------
	--Fiscal - Department
	--ID 50
	--select [TMOCMART.DESCRIPTION].[FUNCTIONCD] from $(STAGING_DB).dbo.[TMOCMART.DEPARTMENTTREE];
	PRINT N'Processing tbl_Maintenance_Activity_UserListItems Department Items...'; 
	---------------------------------------------------------
	MERGE dbo.tbl_Maintenance_Activity_UserListItems AS TARGET 
	using (SELECT DESCRIPTION as item
	FROM [$(STAGING_DB)].[dbo].[DEB_CARDINAL_DEPARTMENTTREE] where [DESCRIPTION] is not null) AS Source 
	ON ( target.item = source.item and target.list_id=50) 
	WHEN matched AND target.item <> source.item THEN 
	UPDATE SET Target.item = source.item 
	WHEN NOT matched BY target THEN 
	INSERT (list_id, item) 
	VALUES ( 50,
		Source.item); 
	select * from dbo.tbl_Maintenance_Activity_UserListItems where list_id = 50;

	---------------------------------------------------------


	PRINT N'Committing transaction...'; 
	COMMIT TRANSACTION [Tran1];

END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION [Tran1];

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

	--TODO: email?

	-- return error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
END CATCH  

PRINT N'...COALoad Complete'; 

END
 
GO


