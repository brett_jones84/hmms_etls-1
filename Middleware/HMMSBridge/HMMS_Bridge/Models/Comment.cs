﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HMMS_Bridge.Models
{
    /// <summary>
    /// VDOT Comments for Service Requests
    /// </summary>
    public class Comment
    {
        /// <summary>
        /// Service Request ID
        /// </summary>
        [Required]
        public string CSC_SR_ID{ get; set; }

        /// <summary>
        /// Comment text
        /// </summary>
        [Required]
        public string CommentText { get; set; }




        /// <summary>
        /// timestamp, used for duplicate detection.
        /// </summary>
        [Required]
        public Nullable<DateTime> TimeStamp { get; set; }

        /// <summary>
        /// when true the api will compare the comment text when updating. 
        /// if the existing comment text matches the incoming comment text the change is discarded.
        /// default behavior is to examine text.
        /// </summary>
        public Boolean useDBForCompare { get; set; }

        /// <summary>
        /// CommentSubject
        /// </summary>
        public string CommentSubject { get; set; }

    }
}